// 3p
// import { hashPassword } from '@foal/core';
import { createConnection } from 'typeorm';
import pokedex from '../datas/pokedex';
import nomFR from '../datas/nomFR';


// App
import { Pokemon, PokemonType } from '../app/entities';

export async function main(/*args*/) {
  const connection = await createConnection();


  let pokemons:Pokemon[] = [];
  let types:PokemonType[] = [];

  try {

    //create types
    for await (const pokemonJSON of pokedex.pokemon) {
        for await (const type of pokemonJSON.type) {
          if(!types[type]){
            let newType = new PokemonType();
            newType.name = type;
            types[type] = newType;
            await newType.save();
          }
        }
        for await (const type of pokemonJSON.weaknesses) {
          if(!types[type]){
            let newType = new PokemonType();
            newType.name = type;
            await newType.save();
            types[type] = newType;
          }
        }
    };
    
    //create pokemons
    for await (const pokemonJSON of pokedex.pokemon) {
      let poke = new Pokemon();
      
      poke.name = nomFR[""+pokemonJSON.id.toString()+""];
      poke.number = pokemonJSON.id;
      poke.image = pokemonJSON.img;
      poke.height = pokemonJSON.height;
      poke.weight = pokemonJSON.weight;
      poke.types = [];
      pokemonJSON.type.forEach(type => {
        poke.types.push(types[type])
      })
      
      poke.weaknessTypes = [];
      pokemonJSON.weaknesses.forEach(type => {
        poke.weaknessTypes.push(types[type])
      })

      await poke.save();
      pokemons[poke.name] = poke;

    };

    
    for await (const pokemonJSON of pokedex.pokemon) {
      let poke:Pokemon = pokemons[nomFR[""+pokemonJSON.id.toString()+""]];
      if(pokemonJSON.next_evolution){
        poke.nextEvolutions = [];
        for await (const evolution of pokemonJSON.next_evolution) {
          poke.nextEvolutions.push(pokemons[nomFR[""+parseInt(evolution.num).toString()+""]]);
        }
      }
      if(pokemonJSON.prev_evolution){
        poke.previousEvolutions = [];
        for await (const evolution of pokemonJSON.prev_evolution) {
          poke.previousEvolutions.push(pokemons[nomFR[""+parseInt(evolution.num.toString())+""]]);
        }
      }

      await poke.save();
    };

  } catch (error) {
    console.log(error.message);
  } finally {
    await connection.close();
  }
}
