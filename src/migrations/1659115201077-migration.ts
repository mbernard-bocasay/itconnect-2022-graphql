import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1659115201077 implements MigrationInterface {
    name = 'migration1659115201077'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "pokemon" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar NOT NULL, "number" integer NOT NULL, "image" varchar NOT NULL, "height" varchar NOT NULL, "weight" varchar NOT NULL)`);
        await queryRunner.query(`CREATE TABLE "pokemon_type" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar NOT NULL)`);
        await queryRunner.query(`CREATE TABLE "user" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL)`);
        await queryRunner.query(`CREATE TABLE "pokemon_previous_evolutions_pokemon" ("pokemonId_1" integer NOT NULL, "pokemonId_2" integer NOT NULL, PRIMARY KEY ("pokemonId_1", "pokemonId_2"))`);
        await queryRunner.query(`CREATE INDEX "IDX_7522f7e4f268fc4cd22298d62e" ON "pokemon_previous_evolutions_pokemon" ("pokemonId_1") `);
        await queryRunner.query(`CREATE INDEX "IDX_0bd5bc7d69688aad3e12b3e4e4" ON "pokemon_previous_evolutions_pokemon" ("pokemonId_2") `);
        await queryRunner.query(`CREATE TABLE "pokemon_next_evolutions_pokemon" ("pokemonId_1" integer NOT NULL, "pokemonId_2" integer NOT NULL, PRIMARY KEY ("pokemonId_1", "pokemonId_2"))`);
        await queryRunner.query(`CREATE INDEX "IDX_258fd20b87242c12f9234b077a" ON "pokemon_next_evolutions_pokemon" ("pokemonId_1") `);
        await queryRunner.query(`CREATE INDEX "IDX_f885c02811e20bdd2aa9e9d98d" ON "pokemon_next_evolutions_pokemon" ("pokemonId_2") `);
        await queryRunner.query(`CREATE TABLE "pokemon_types_pokemon_type" ("pokemonId" integer NOT NULL, "pokemonTypeId" integer NOT NULL, PRIMARY KEY ("pokemonId", "pokemonTypeId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_820c2d08066d1242d6de9773e3" ON "pokemon_types_pokemon_type" ("pokemonId") `);
        await queryRunner.query(`CREATE INDEX "IDX_c59628b892395cb33f56249ee4" ON "pokemon_types_pokemon_type" ("pokemonTypeId") `);
        await queryRunner.query(`CREATE TABLE "pokemon_weakness_types_pokemon_type" ("pokemonId" integer NOT NULL, "pokemonTypeId" integer NOT NULL, PRIMARY KEY ("pokemonId", "pokemonTypeId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_3c4cc7d0e9895ae102438aa48d" ON "pokemon_weakness_types_pokemon_type" ("pokemonId") `);
        await queryRunner.query(`CREATE INDEX "IDX_830bb007e90fc2e0f0d962b8ff" ON "pokemon_weakness_types_pokemon_type" ("pokemonTypeId") `);
        await queryRunner.query(`CREATE TABLE "pokemon_type_pokemons_pokemon" ("pokemonTypeId" integer NOT NULL, "pokemonId" integer NOT NULL, PRIMARY KEY ("pokemonTypeId", "pokemonId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_4631603c42f6484f1099c8b931" ON "pokemon_type_pokemons_pokemon" ("pokemonTypeId") `);
        await queryRunner.query(`CREATE INDEX "IDX_e0184d3a02c5fb3ee1a08dadb7" ON "pokemon_type_pokemons_pokemon" ("pokemonId") `);
        await queryRunner.query(`CREATE TABLE "pokemon_type_weak_pokemons_pokemon" ("pokemonTypeId" integer NOT NULL, "pokemonId" integer NOT NULL, PRIMARY KEY ("pokemonTypeId", "pokemonId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_71c184f9a13b464bf8a261f591" ON "pokemon_type_weak_pokemons_pokemon" ("pokemonTypeId") `);
        await queryRunner.query(`CREATE INDEX "IDX_3adce88167f79c5dc27748aa35" ON "pokemon_type_weak_pokemons_pokemon" ("pokemonId") `);
        await queryRunner.query(`DROP INDEX "IDX_7522f7e4f268fc4cd22298d62e"`);
        await queryRunner.query(`DROP INDEX "IDX_0bd5bc7d69688aad3e12b3e4e4"`);
        await queryRunner.query(`CREATE TABLE "temporary_pokemon_previous_evolutions_pokemon" ("pokemonId_1" integer NOT NULL, "pokemonId_2" integer NOT NULL, CONSTRAINT "FK_7522f7e4f268fc4cd22298d62e1" FOREIGN KEY ("pokemonId_1") REFERENCES "pokemon" ("id") ON DELETE CASCADE ON UPDATE NO ACTION, CONSTRAINT "FK_0bd5bc7d69688aad3e12b3e4e45" FOREIGN KEY ("pokemonId_2") REFERENCES "pokemon" ("id") ON DELETE CASCADE ON UPDATE NO ACTION, PRIMARY KEY ("pokemonId_1", "pokemonId_2"))`);
        await queryRunner.query(`INSERT INTO "temporary_pokemon_previous_evolutions_pokemon"("pokemonId_1", "pokemonId_2") SELECT "pokemonId_1", "pokemonId_2" FROM "pokemon_previous_evolutions_pokemon"`);
        await queryRunner.query(`DROP TABLE "pokemon_previous_evolutions_pokemon"`);
        await queryRunner.query(`ALTER TABLE "temporary_pokemon_previous_evolutions_pokemon" RENAME TO "pokemon_previous_evolutions_pokemon"`);
        await queryRunner.query(`CREATE INDEX "IDX_7522f7e4f268fc4cd22298d62e" ON "pokemon_previous_evolutions_pokemon" ("pokemonId_1") `);
        await queryRunner.query(`CREATE INDEX "IDX_0bd5bc7d69688aad3e12b3e4e4" ON "pokemon_previous_evolutions_pokemon" ("pokemonId_2") `);
        await queryRunner.query(`DROP INDEX "IDX_258fd20b87242c12f9234b077a"`);
        await queryRunner.query(`DROP INDEX "IDX_f885c02811e20bdd2aa9e9d98d"`);
        await queryRunner.query(`CREATE TABLE "temporary_pokemon_next_evolutions_pokemon" ("pokemonId_1" integer NOT NULL, "pokemonId_2" integer NOT NULL, CONSTRAINT "FK_258fd20b87242c12f9234b077a9" FOREIGN KEY ("pokemonId_1") REFERENCES "pokemon" ("id") ON DELETE CASCADE ON UPDATE NO ACTION, CONSTRAINT "FK_f885c02811e20bdd2aa9e9d98dc" FOREIGN KEY ("pokemonId_2") REFERENCES "pokemon" ("id") ON DELETE CASCADE ON UPDATE NO ACTION, PRIMARY KEY ("pokemonId_1", "pokemonId_2"))`);
        await queryRunner.query(`INSERT INTO "temporary_pokemon_next_evolutions_pokemon"("pokemonId_1", "pokemonId_2") SELECT "pokemonId_1", "pokemonId_2" FROM "pokemon_next_evolutions_pokemon"`);
        await queryRunner.query(`DROP TABLE "pokemon_next_evolutions_pokemon"`);
        await queryRunner.query(`ALTER TABLE "temporary_pokemon_next_evolutions_pokemon" RENAME TO "pokemon_next_evolutions_pokemon"`);
        await queryRunner.query(`CREATE INDEX "IDX_258fd20b87242c12f9234b077a" ON "pokemon_next_evolutions_pokemon" ("pokemonId_1") `);
        await queryRunner.query(`CREATE INDEX "IDX_f885c02811e20bdd2aa9e9d98d" ON "pokemon_next_evolutions_pokemon" ("pokemonId_2") `);
        await queryRunner.query(`DROP INDEX "IDX_820c2d08066d1242d6de9773e3"`);
        await queryRunner.query(`DROP INDEX "IDX_c59628b892395cb33f56249ee4"`);
        await queryRunner.query(`CREATE TABLE "temporary_pokemon_types_pokemon_type" ("pokemonId" integer NOT NULL, "pokemonTypeId" integer NOT NULL, CONSTRAINT "FK_820c2d08066d1242d6de9773e36" FOREIGN KEY ("pokemonId") REFERENCES "pokemon" ("id") ON DELETE CASCADE ON UPDATE NO ACTION, CONSTRAINT "FK_c59628b892395cb33f56249ee4c" FOREIGN KEY ("pokemonTypeId") REFERENCES "pokemon_type" ("id") ON DELETE CASCADE ON UPDATE NO ACTION, PRIMARY KEY ("pokemonId", "pokemonTypeId"))`);
        await queryRunner.query(`INSERT INTO "temporary_pokemon_types_pokemon_type"("pokemonId", "pokemonTypeId") SELECT "pokemonId", "pokemonTypeId" FROM "pokemon_types_pokemon_type"`);
        await queryRunner.query(`DROP TABLE "pokemon_types_pokemon_type"`);
        await queryRunner.query(`ALTER TABLE "temporary_pokemon_types_pokemon_type" RENAME TO "pokemon_types_pokemon_type"`);
        await queryRunner.query(`CREATE INDEX "IDX_820c2d08066d1242d6de9773e3" ON "pokemon_types_pokemon_type" ("pokemonId") `);
        await queryRunner.query(`CREATE INDEX "IDX_c59628b892395cb33f56249ee4" ON "pokemon_types_pokemon_type" ("pokemonTypeId") `);
        await queryRunner.query(`DROP INDEX "IDX_3c4cc7d0e9895ae102438aa48d"`);
        await queryRunner.query(`DROP INDEX "IDX_830bb007e90fc2e0f0d962b8ff"`);
        await queryRunner.query(`CREATE TABLE "temporary_pokemon_weakness_types_pokemon_type" ("pokemonId" integer NOT NULL, "pokemonTypeId" integer NOT NULL, CONSTRAINT "FK_3c4cc7d0e9895ae102438aa48d8" FOREIGN KEY ("pokemonId") REFERENCES "pokemon" ("id") ON DELETE CASCADE ON UPDATE NO ACTION, CONSTRAINT "FK_830bb007e90fc2e0f0d962b8ffd" FOREIGN KEY ("pokemonTypeId") REFERENCES "pokemon_type" ("id") ON DELETE CASCADE ON UPDATE NO ACTION, PRIMARY KEY ("pokemonId", "pokemonTypeId"))`);
        await queryRunner.query(`INSERT INTO "temporary_pokemon_weakness_types_pokemon_type"("pokemonId", "pokemonTypeId") SELECT "pokemonId", "pokemonTypeId" FROM "pokemon_weakness_types_pokemon_type"`);
        await queryRunner.query(`DROP TABLE "pokemon_weakness_types_pokemon_type"`);
        await queryRunner.query(`ALTER TABLE "temporary_pokemon_weakness_types_pokemon_type" RENAME TO "pokemon_weakness_types_pokemon_type"`);
        await queryRunner.query(`CREATE INDEX "IDX_3c4cc7d0e9895ae102438aa48d" ON "pokemon_weakness_types_pokemon_type" ("pokemonId") `);
        await queryRunner.query(`CREATE INDEX "IDX_830bb007e90fc2e0f0d962b8ff" ON "pokemon_weakness_types_pokemon_type" ("pokemonTypeId") `);
        await queryRunner.query(`DROP INDEX "IDX_4631603c42f6484f1099c8b931"`);
        await queryRunner.query(`DROP INDEX "IDX_e0184d3a02c5fb3ee1a08dadb7"`);
        await queryRunner.query(`CREATE TABLE "temporary_pokemon_type_pokemons_pokemon" ("pokemonTypeId" integer NOT NULL, "pokemonId" integer NOT NULL, CONSTRAINT "FK_4631603c42f6484f1099c8b9313" FOREIGN KEY ("pokemonTypeId") REFERENCES "pokemon_type" ("id") ON DELETE CASCADE ON UPDATE NO ACTION, CONSTRAINT "FK_e0184d3a02c5fb3ee1a08dadb7a" FOREIGN KEY ("pokemonId") REFERENCES "pokemon" ("id") ON DELETE CASCADE ON UPDATE NO ACTION, PRIMARY KEY ("pokemonTypeId", "pokemonId"))`);
        await queryRunner.query(`INSERT INTO "temporary_pokemon_type_pokemons_pokemon"("pokemonTypeId", "pokemonId") SELECT "pokemonTypeId", "pokemonId" FROM "pokemon_type_pokemons_pokemon"`);
        await queryRunner.query(`DROP TABLE "pokemon_type_pokemons_pokemon"`);
        await queryRunner.query(`ALTER TABLE "temporary_pokemon_type_pokemons_pokemon" RENAME TO "pokemon_type_pokemons_pokemon"`);
        await queryRunner.query(`CREATE INDEX "IDX_4631603c42f6484f1099c8b931" ON "pokemon_type_pokemons_pokemon" ("pokemonTypeId") `);
        await queryRunner.query(`CREATE INDEX "IDX_e0184d3a02c5fb3ee1a08dadb7" ON "pokemon_type_pokemons_pokemon" ("pokemonId") `);
        await queryRunner.query(`DROP INDEX "IDX_71c184f9a13b464bf8a261f591"`);
        await queryRunner.query(`DROP INDEX "IDX_3adce88167f79c5dc27748aa35"`);
        await queryRunner.query(`CREATE TABLE "temporary_pokemon_type_weak_pokemons_pokemon" ("pokemonTypeId" integer NOT NULL, "pokemonId" integer NOT NULL, CONSTRAINT "FK_71c184f9a13b464bf8a261f5911" FOREIGN KEY ("pokemonTypeId") REFERENCES "pokemon_type" ("id") ON DELETE CASCADE ON UPDATE NO ACTION, CONSTRAINT "FK_3adce88167f79c5dc27748aa35d" FOREIGN KEY ("pokemonId") REFERENCES "pokemon" ("id") ON DELETE CASCADE ON UPDATE NO ACTION, PRIMARY KEY ("pokemonTypeId", "pokemonId"))`);
        await queryRunner.query(`INSERT INTO "temporary_pokemon_type_weak_pokemons_pokemon"("pokemonTypeId", "pokemonId") SELECT "pokemonTypeId", "pokemonId" FROM "pokemon_type_weak_pokemons_pokemon"`);
        await queryRunner.query(`DROP TABLE "pokemon_type_weak_pokemons_pokemon"`);
        await queryRunner.query(`ALTER TABLE "temporary_pokemon_type_weak_pokemons_pokemon" RENAME TO "pokemon_type_weak_pokemons_pokemon"`);
        await queryRunner.query(`CREATE INDEX "IDX_71c184f9a13b464bf8a261f591" ON "pokemon_type_weak_pokemons_pokemon" ("pokemonTypeId") `);
        await queryRunner.query(`CREATE INDEX "IDX_3adce88167f79c5dc27748aa35" ON "pokemon_type_weak_pokemons_pokemon" ("pokemonId") `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "IDX_3adce88167f79c5dc27748aa35"`);
        await queryRunner.query(`DROP INDEX "IDX_71c184f9a13b464bf8a261f591"`);
        await queryRunner.query(`ALTER TABLE "pokemon_type_weak_pokemons_pokemon" RENAME TO "temporary_pokemon_type_weak_pokemons_pokemon"`);
        await queryRunner.query(`CREATE TABLE "pokemon_type_weak_pokemons_pokemon" ("pokemonTypeId" integer NOT NULL, "pokemonId" integer NOT NULL, PRIMARY KEY ("pokemonTypeId", "pokemonId"))`);
        await queryRunner.query(`INSERT INTO "pokemon_type_weak_pokemons_pokemon"("pokemonTypeId", "pokemonId") SELECT "pokemonTypeId", "pokemonId" FROM "temporary_pokemon_type_weak_pokemons_pokemon"`);
        await queryRunner.query(`DROP TABLE "temporary_pokemon_type_weak_pokemons_pokemon"`);
        await queryRunner.query(`CREATE INDEX "IDX_3adce88167f79c5dc27748aa35" ON "pokemon_type_weak_pokemons_pokemon" ("pokemonId") `);
        await queryRunner.query(`CREATE INDEX "IDX_71c184f9a13b464bf8a261f591" ON "pokemon_type_weak_pokemons_pokemon" ("pokemonTypeId") `);
        await queryRunner.query(`DROP INDEX "IDX_e0184d3a02c5fb3ee1a08dadb7"`);
        await queryRunner.query(`DROP INDEX "IDX_4631603c42f6484f1099c8b931"`);
        await queryRunner.query(`ALTER TABLE "pokemon_type_pokemons_pokemon" RENAME TO "temporary_pokemon_type_pokemons_pokemon"`);
        await queryRunner.query(`CREATE TABLE "pokemon_type_pokemons_pokemon" ("pokemonTypeId" integer NOT NULL, "pokemonId" integer NOT NULL, PRIMARY KEY ("pokemonTypeId", "pokemonId"))`);
        await queryRunner.query(`INSERT INTO "pokemon_type_pokemons_pokemon"("pokemonTypeId", "pokemonId") SELECT "pokemonTypeId", "pokemonId" FROM "temporary_pokemon_type_pokemons_pokemon"`);
        await queryRunner.query(`DROP TABLE "temporary_pokemon_type_pokemons_pokemon"`);
        await queryRunner.query(`CREATE INDEX "IDX_e0184d3a02c5fb3ee1a08dadb7" ON "pokemon_type_pokemons_pokemon" ("pokemonId") `);
        await queryRunner.query(`CREATE INDEX "IDX_4631603c42f6484f1099c8b931" ON "pokemon_type_pokemons_pokemon" ("pokemonTypeId") `);
        await queryRunner.query(`DROP INDEX "IDX_830bb007e90fc2e0f0d962b8ff"`);
        await queryRunner.query(`DROP INDEX "IDX_3c4cc7d0e9895ae102438aa48d"`);
        await queryRunner.query(`ALTER TABLE "pokemon_weakness_types_pokemon_type" RENAME TO "temporary_pokemon_weakness_types_pokemon_type"`);
        await queryRunner.query(`CREATE TABLE "pokemon_weakness_types_pokemon_type" ("pokemonId" integer NOT NULL, "pokemonTypeId" integer NOT NULL, PRIMARY KEY ("pokemonId", "pokemonTypeId"))`);
        await queryRunner.query(`INSERT INTO "pokemon_weakness_types_pokemon_type"("pokemonId", "pokemonTypeId") SELECT "pokemonId", "pokemonTypeId" FROM "temporary_pokemon_weakness_types_pokemon_type"`);
        await queryRunner.query(`DROP TABLE "temporary_pokemon_weakness_types_pokemon_type"`);
        await queryRunner.query(`CREATE INDEX "IDX_830bb007e90fc2e0f0d962b8ff" ON "pokemon_weakness_types_pokemon_type" ("pokemonTypeId") `);
        await queryRunner.query(`CREATE INDEX "IDX_3c4cc7d0e9895ae102438aa48d" ON "pokemon_weakness_types_pokemon_type" ("pokemonId") `);
        await queryRunner.query(`DROP INDEX "IDX_c59628b892395cb33f56249ee4"`);
        await queryRunner.query(`DROP INDEX "IDX_820c2d08066d1242d6de9773e3"`);
        await queryRunner.query(`ALTER TABLE "pokemon_types_pokemon_type" RENAME TO "temporary_pokemon_types_pokemon_type"`);
        await queryRunner.query(`CREATE TABLE "pokemon_types_pokemon_type" ("pokemonId" integer NOT NULL, "pokemonTypeId" integer NOT NULL, PRIMARY KEY ("pokemonId", "pokemonTypeId"))`);
        await queryRunner.query(`INSERT INTO "pokemon_types_pokemon_type"("pokemonId", "pokemonTypeId") SELECT "pokemonId", "pokemonTypeId" FROM "temporary_pokemon_types_pokemon_type"`);
        await queryRunner.query(`DROP TABLE "temporary_pokemon_types_pokemon_type"`);
        await queryRunner.query(`CREATE INDEX "IDX_c59628b892395cb33f56249ee4" ON "pokemon_types_pokemon_type" ("pokemonTypeId") `);
        await queryRunner.query(`CREATE INDEX "IDX_820c2d08066d1242d6de9773e3" ON "pokemon_types_pokemon_type" ("pokemonId") `);
        await queryRunner.query(`DROP INDEX "IDX_f885c02811e20bdd2aa9e9d98d"`);
        await queryRunner.query(`DROP INDEX "IDX_258fd20b87242c12f9234b077a"`);
        await queryRunner.query(`ALTER TABLE "pokemon_next_evolutions_pokemon" RENAME TO "temporary_pokemon_next_evolutions_pokemon"`);
        await queryRunner.query(`CREATE TABLE "pokemon_next_evolutions_pokemon" ("pokemonId_1" integer NOT NULL, "pokemonId_2" integer NOT NULL, PRIMARY KEY ("pokemonId_1", "pokemonId_2"))`);
        await queryRunner.query(`INSERT INTO "pokemon_next_evolutions_pokemon"("pokemonId_1", "pokemonId_2") SELECT "pokemonId_1", "pokemonId_2" FROM "temporary_pokemon_next_evolutions_pokemon"`);
        await queryRunner.query(`DROP TABLE "temporary_pokemon_next_evolutions_pokemon"`);
        await queryRunner.query(`CREATE INDEX "IDX_f885c02811e20bdd2aa9e9d98d" ON "pokemon_next_evolutions_pokemon" ("pokemonId_2") `);
        await queryRunner.query(`CREATE INDEX "IDX_258fd20b87242c12f9234b077a" ON "pokemon_next_evolutions_pokemon" ("pokemonId_1") `);
        await queryRunner.query(`DROP INDEX "IDX_0bd5bc7d69688aad3e12b3e4e4"`);
        await queryRunner.query(`DROP INDEX "IDX_7522f7e4f268fc4cd22298d62e"`);
        await queryRunner.query(`ALTER TABLE "pokemon_previous_evolutions_pokemon" RENAME TO "temporary_pokemon_previous_evolutions_pokemon"`);
        await queryRunner.query(`CREATE TABLE "pokemon_previous_evolutions_pokemon" ("pokemonId_1" integer NOT NULL, "pokemonId_2" integer NOT NULL, PRIMARY KEY ("pokemonId_1", "pokemonId_2"))`);
        await queryRunner.query(`INSERT INTO "pokemon_previous_evolutions_pokemon"("pokemonId_1", "pokemonId_2") SELECT "pokemonId_1", "pokemonId_2" FROM "temporary_pokemon_previous_evolutions_pokemon"`);
        await queryRunner.query(`DROP TABLE "temporary_pokemon_previous_evolutions_pokemon"`);
        await queryRunner.query(`CREATE INDEX "IDX_0bd5bc7d69688aad3e12b3e4e4" ON "pokemon_previous_evolutions_pokemon" ("pokemonId_2") `);
        await queryRunner.query(`CREATE INDEX "IDX_7522f7e4f268fc4cd22298d62e" ON "pokemon_previous_evolutions_pokemon" ("pokemonId_1") `);
        await queryRunner.query(`DROP INDEX "IDX_3adce88167f79c5dc27748aa35"`);
        await queryRunner.query(`DROP INDEX "IDX_71c184f9a13b464bf8a261f591"`);
        await queryRunner.query(`DROP TABLE "pokemon_type_weak_pokemons_pokemon"`);
        await queryRunner.query(`DROP INDEX "IDX_e0184d3a02c5fb3ee1a08dadb7"`);
        await queryRunner.query(`DROP INDEX "IDX_4631603c42f6484f1099c8b931"`);
        await queryRunner.query(`DROP TABLE "pokemon_type_pokemons_pokemon"`);
        await queryRunner.query(`DROP INDEX "IDX_830bb007e90fc2e0f0d962b8ff"`);
        await queryRunner.query(`DROP INDEX "IDX_3c4cc7d0e9895ae102438aa48d"`);
        await queryRunner.query(`DROP TABLE "pokemon_weakness_types_pokemon_type"`);
        await queryRunner.query(`DROP INDEX "IDX_c59628b892395cb33f56249ee4"`);
        await queryRunner.query(`DROP INDEX "IDX_820c2d08066d1242d6de9773e3"`);
        await queryRunner.query(`DROP TABLE "pokemon_types_pokemon_type"`);
        await queryRunner.query(`DROP INDEX "IDX_f885c02811e20bdd2aa9e9d98d"`);
        await queryRunner.query(`DROP INDEX "IDX_258fd20b87242c12f9234b077a"`);
        await queryRunner.query(`DROP TABLE "pokemon_next_evolutions_pokemon"`);
        await queryRunner.query(`DROP INDEX "IDX_0bd5bc7d69688aad3e12b3e4e4"`);
        await queryRunner.query(`DROP INDEX "IDX_7522f7e4f268fc4cd22298d62e"`);
        await queryRunner.query(`DROP TABLE "pokemon_previous_evolutions_pokemon"`);
        await queryRunner.query(`DROP TABLE "user"`);
        await queryRunner.query(`DROP TABLE "pokemon_type"`);
        await queryRunner.query(`DROP TABLE "pokemon"`);
    }

}
