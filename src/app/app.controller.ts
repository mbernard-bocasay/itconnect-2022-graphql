import { Context, controller, Hook, HttpResponseNoContent, IAppController, Options } from '@foal/core';
import { GraphiQLController } from '@foal/graphiql';
import { createConnection } from 'typeorm';

import { ApiController } from './controllers';
import { GraphController } from './controllers/graph.controller';
import { swaggerController } from './controllers/restapi/swagger.controller';


@Hook(() => response => {
  // Every response of this controller and its sub-controllers will be added this header.
  response.setHeader('Access-Control-Allow-Origin', '*');
})
export class AppController implements IAppController {
  subControllers = [
    controller('/api', ApiController),
    controller('/graphql', GraphController),
    controller('/graphiql', GraphiQLController),
    controller('/swagger', swaggerController),
  ];

  @Options('*')
  options(ctx: Context) {
    const response = new HttpResponseNoContent();
    response.setHeader('Access-Control-Allow-Methods', 'HEAD, GET, POST, PUT, PATCH, DELETE');
    // You may need to allow other headers depending on what you need.
    response.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    return response;
  }


  async init() {
    await createConnection();
  }
}
