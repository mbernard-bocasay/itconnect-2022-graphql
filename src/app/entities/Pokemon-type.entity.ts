// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { BaseEntity, Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';
import { PokemonService } from '../services';
import { Pokemon } from './Pokemon.entity';

@Entity()
export class PokemonType extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name:string;

  @ManyToMany(() => Pokemon, (pokemon) => pokemon.types)
  @JoinTable()
  pokemons: Pokemon[];

  
  async type_pokemons (){
    let pokemonService = new PokemonService();
    let pokemons:Pokemon[] = [];
    for await (const pokemon of this.pokemons) {
      pokemons.push(await pokemonService.getPokemon(pokemon.id))
    }
    return pokemons;
  }
  
  @ManyToMany(() => Pokemon, (pokemon) => pokemon.weaknessTypes)
  @JoinTable()
  weakPokemons: Pokemon[]; 

  async weak_pokemons (){
    let pokemonService = new PokemonService();
    let pokemons:Pokemon[] = [];
    for await (const pokemon of this.weakPokemons) {
      pokemons.push(await pokemonService.getPokemon(pokemon.id))
    }
    return pokemons;
  }

  toURL(){
    return '/api/type/'+this.id;
  }
}
export interface IPokemonType{
  id:number;
  name:string;
  type_pokemons?: string[];
  weak_pokemons?: string[];
  uri?:string;
}
