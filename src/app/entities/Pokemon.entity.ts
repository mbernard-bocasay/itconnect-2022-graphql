// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { BaseEntity, Column, Entity, JoinColumn, JoinTable, ManyToMany, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { PokemonService, PokemonTypeService } from '../services';
import { PokemonType } from './Pokemon-type.entity';

@Entity()
export class Pokemon extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name:string;

  @Column()
  number:number;

  @Column()
  image:string;

  @Column()
  height:string;
  
  @Column()
  weight:string;

  @ManyToMany(() => Pokemon, { nullable: false})
  @JoinTable()
  previousEvolutions: Pokemon[];

  async previous_evolutions (){
    let pokemonService = new PokemonService();
    let evolutions:Pokemon[] = [];
    for await (const evolution of this.previousEvolutions) {
      evolutions.push(await pokemonService.getPokemon(evolution.id))
    }
    return evolutions;
  }

  @ManyToMany(() => Pokemon, { nullable: false })
  @JoinTable()
  nextEvolutions: Pokemon[];
  
  
  async next_evolutions (){
    let pokemonService = new PokemonService();
    let evolutions:Pokemon[] = [];
    for await (const evolution of this.nextEvolutions) {
      evolutions.push(await pokemonService.getPokemon(evolution.id))
    }
    return evolutions;
  }


  @ManyToMany(() => PokemonType, (type) => type.pokemons, { nullable: false })
  @JoinTable()
  types: PokemonType[];

  
  async pokemon_types (){
    let pokemonTypeService = new PokemonTypeService();
    let poketypes:PokemonType[] = [];
    for await (const type of this.types) {
      poketypes.push(await pokemonTypeService.getPokemonType(type.id))
    }
    return poketypes;
  }


  @ManyToMany(() => PokemonType, (type) => type.weakPokemons, { nullable: false})
  @JoinTable()
  weaknessTypes: PokemonType[];

  
  async pokemon_weakness_types (){
    let pokemonTypeService = new PokemonTypeService();
    let poketypes:PokemonType[] = [];
    for await (const type of this.weaknessTypes) {
      poketypes.push(await pokemonTypeService.getPokemonType(type.id))
    }
    return poketypes;
  }
  toURL(){
    return '/api/pokemon/'+this.id;
  }
}

export interface IPokemon{
  name:string;

  number:number;

  image?:string;

  height?:string;
  
  weight?:string;

  previous_evolutions?: string[];
  next_evolutions?: string[];
  types?: string[];
  weaknessTypes?: string[];
  uri?:string;
}
