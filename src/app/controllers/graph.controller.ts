import { GraphQLController } from '@foal/graphql';
import resolvers from './graphql/resolvers';
import schema from './graphql/schema';

export class GraphController  extends GraphQLController {
    schema = schema;
    resolvers = resolvers;
}
