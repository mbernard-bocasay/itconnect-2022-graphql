import { ApiInfo, ApiServer, Context, controller, Get, HttpResponseOK } from '@foal/core';
import { Pokemon } from '../entities';
import { PokemonController } from './restapi/pokemon.controller';
import { PokemonTypeController } from './restapi/pokemontype.controller';

@ApiInfo({
  title: 'Pokemon REST API',
  version: '1.0.0'
})
@ApiServer({
  url: '/api'
})
export class ApiController {
  subControllers = [
    controller('/pokemon', PokemonController),
    controller('/type', PokemonTypeController),
  ];

}
