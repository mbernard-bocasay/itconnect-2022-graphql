import { buildSchema } from 'graphql';

const schema = buildSchema(`
  "API GraphQL Pokemon Generation 1"
  type Query {
    "Retourne tout les pokemons matchant avec le nom"
    pokemons(name: String): [Pokemon]
    "Retourne le pokemon ayant l'id envoyé"
    pokemon(id: Int!): Pokemon
    "Retourne tout les types de pokemon matchant avec le nom"
    types: [PokemonType]
    "Retourne le type de pokemon ayant l'id envoyé"
    type(id: Int!): PokemonType
  }

  """
  Un objet Pokemon
  """
  type Pokemon {
    id: Int!
    name:String!
    number:Int!
    image:String!
    height:String!
    weight:String!
    previous_evolutions: [Pokemon]
    next_evolutions: [Pokemon]
    pokemon_types: [PokemonType]
    pokemon_weakness_types: [PokemonType]
    toURL: String
  }

  """
  Un type de Pokemon
  """
  type PokemonType {
    id: Int!
    name: String!
    type_pokemons: [Pokemon]
    weak_pokemons: [Pokemon]
  }
`);

export default schema;