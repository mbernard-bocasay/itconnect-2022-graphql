import { HttpResponseOK } from "@foal/core";
import { Pokemon } from "../../entities";
import { sleep } from "../../helpers/time.helper";
import { PokemonService, PokemonTypeService } from "../../services";

const pokemonService:PokemonService = new PokemonService();
const pokemonTypeService:PokemonTypeService = new PokemonTypeService();



const resolvers = {
    pokemons: async (args) => {
        await sleep(15);
        return await pokemonService.getPokemons(args.name);
    },
    pokemon: async (args) => {
        await sleep(15);
        return await pokemonService.getPokemon(args.id);
    },
    types: async (args) => {
        await sleep(15);
        return await pokemonTypeService.getPokemonTypes(args.name);
    },
    type: async (args) => {
        await sleep(15);
        return await pokemonTypeService.getPokemonType(args.id);
    },

  };

export default resolvers;