import { Context, dependency, Get, HttpResponseNotFound, HttpResponseOK } from '@foal/core';
import { PokemonType } from '../../entities';
import { IPokemonType } from '../../entities/Pokemon-type.entity';
import { sleep } from '../../helpers/time.helper';
import { PokemonTypeService } from '../../services';

export class PokemonTypeController {

  @dependency
  pokemonTypeService:PokemonTypeService
  
  @Get('/')
  async types(ctx: Context) {
    await sleep(15);
    let pokemontypes = await this.pokemonTypeService.getPokemonTypes(ctx.request.query.name);
    return new HttpResponseOK({data:this.TypeToIType(pokemontypes)});
  }

  @Get('/:id')
  async type(ctx: Context) {
    await sleep(15);
    let type = await this.pokemonTypeService.getPokemonType(ctx.request.params.id);
    if(type)
      return new HttpResponseOK({data:this.TypeToIType(type)});
    else
      return new HttpResponseNotFound();
  }

  private TypeToIType(type:PokemonType[]|PokemonType):IPokemonType[]|IPokemonType{
    let retour:IPokemonType[]|IPokemonType;
    if(!Array.isArray(type)){
      retour = {
        id: type.id,
        name:type.name,
        type_pokemons: type.pokemons?.map(p => p.toURL()),
        weak_pokemons: type.weakPokemons?.map(p=> p.toURL())
      }
    }else{
      retour = []
      type.forEach(t => {
        (retour as IPokemonType[]).push({
          id: t.id,
          name:t.name,
          uri: t.toURL()
        })
      })
    }
    return retour;
  }

}
