import { Context, dependency, Get, HttpResponseNotFound, HttpResponseOK } from '@foal/core';
import { Pokemon, PokemonType } from '../../entities';
import { IPokemon } from '../../entities/Pokemon.entity';
import { sleep } from '../../helpers/time.helper';
import { PokemonService } from '../../services';

export class PokemonController {

  @dependency
  pokemonService:PokemonService

  @Get('/')
  async pokemons(ctx: Context) {
    await sleep(15);
    const pokemons = await this.pokemonService.getPokemons(ctx.request.query.name);
    return new HttpResponseOK({data:this.PokemonsToIPokemons(pokemons)});
  }

  
  @Get('/:id')
  async pokemon(ctx: Context) {
    await sleep(15);
   const pokemon = await this.pokemonService.getPokemon(ctx.request.params.id)
    if(pokemon)
      return new HttpResponseOK({data:this.PokemonsToIPokemons(pokemon)});
    else
      return new HttpResponseNotFound();
  }

  
  @Get('/simple/:id')
  async pokemonSimple(ctx: Context) {
    await sleep(15);
   const pokemon = await this.pokemonService.getPokemon(ctx.request.params.id)
    if(pokemon)
      return new HttpResponseOK({data:{
        number:pokemon.number,
        name: pokemon.name,
        image:pokemon.image,
      }});
    else
      return new HttpResponseNotFound();
  }

  private PokemonsToIPokemons(pokemon:Pokemon[]|Pokemon):IPokemon[]|IPokemon{
    let retour:IPokemon[]|IPokemon;
    if(!Array.isArray(pokemon)){
      retour = {
        number:pokemon.number,
        name: pokemon.name,
        image:pokemon.image,
        height: pokemon.height,
        weight: pokemon.weight,
        types: pokemon.types?.map(type=> type.toURL()),
        weaknessTypes: pokemon.weaknessTypes?.map(type=> type.toURL()),
        next_evolutions: pokemon.nextEvolutions?.map(p=> p.toURL()),
        previous_evolutions: pokemon.previousEvolutions?.map(p=> p.toURL())
      }
    }else{
      retour = []
      pokemon.forEach(poke => {
        (retour as IPokemon[]).push({
          number:poke.number,
          name: poke.name,
          uri: poke.toURL()
          
        })
      })
    }
    return retour;
  }

}
