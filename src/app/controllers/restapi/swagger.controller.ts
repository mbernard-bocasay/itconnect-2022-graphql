import { SwaggerController } from '@foal/swagger';

import { ApiController } from '../api.controller';

export class swaggerController extends SwaggerController {
  options = { controllerClass: ApiController };
}