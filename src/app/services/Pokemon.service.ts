import { Pokemon } from "../entities";

export class PokemonService {
    async getPokemons(search:string|null):Promise<Pokemon[]>{
        let pokemons:any = await Pokemon.find({
            relations: ['nextEvolutions','previousEvolutions','types','weaknessTypes']
          });
          if(search){
            search = search.toLowerCase();
            pokemons = pokemons.filter(pokemon => {
             return pokemon.name.toLowerCase().includes(search) ? pokemon:null;
            })  
          }
        return pokemons;
    }

    async getPokemon(id:number):Promise<Pokemon>{
        let pokemon = await Pokemon.findOne({'number': id},{
            relations: ['nextEvolutions','previousEvolutions','types','weaknessTypes']
          });
        return pokemon as Pokemon;
    }
}
