import { PokemonType } from "../entities";

export class PokemonTypeService {
    async getPokemonTypes(search:string|null):Promise<PokemonType[]>{
        let pokemonTypes:any = await PokemonType.find({
            relations: ['pokemons', 'weakPokemons']
          });
          if(search){
            search = search.toLowerCase();
            pokemonTypes = pokemonTypes.filter(type => {
             return type.name.toLowerCase().includes(search) ? type:null;
            })  
          }
        return pokemonTypes;
    }

    async getPokemonType(id:number):Promise<PokemonType>{
        let types = await PokemonType.findOne({'id': id},{
            relations: ['pokemons', 'weakPokemons']
          });
        return types as PokemonType;
    }
}
