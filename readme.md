# IT CONNECT 2022
## Introduction à GraphQL par Mathieu BERNARD 

Projet de démonstration mettant en avant les différences entre une API REST et une API GraphQL dans l'obtention de résultats sous plusieurs niveaux

---

#### BACKEND :
---

**Technologies** :
- FoalTS : https://foalts.org/
- GraphQL : https://graphql.org/
- Typescript : https://www.typescriptlang.org/

**Installation des dépendances** :   
`npm install -g @foal/cli`      
`npm install`

**Préparation des données** :   
`npm run build`   
`npm run migrations`   
`foal run create-pokemons`

**Lancer le serveur** :
`npm run dev`


**Point d'entrée API REST** :   
- pokemons : `http://localhost:3010/api/pokemon`   
- types de pokemon : `http://localhost:3010/api/type`


**Point d'entrée API GraphQL** :
`http://localhost:3010/graphql`


**Point d'entrée editeur graphiQL** :
`http://localhost:3010/graphiql`

---
#### FRONTEND :
---

**Technologies** :
- React : https://fr.reactjs.org/
- ViteJS : https://vitejs.dev/
- Typescript : https://www.typescriptlang.org/
- Axios : https://axios-http.com/

**Dossier** :
`cd front`


**Installation des dépendances** :
`npm install`


**Lancer le serveur** :
`npm run dev`