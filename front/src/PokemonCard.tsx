import React, { useEffect, useState } from 'react';
import LoadingImage from './LoadingImage';

export interface PokemonCardInterface {
    url?:string
    name?:string
    onClick?:() => void
}

const PokemonCard:React.FunctionComponent<PokemonCardInterface> = (props) => {

    return (
        <div style={{width:'200px',border:'2px solid white', borderRadius:'15px', margin: '15px', 'boxShadow': '5px 5px 5px #000000', cursor: props.onClick ? 'pointer':'auto'}} onClick={(e) => {
            if(props.onClick) props.onClick()
        }}>
            <h2>{props.name}</h2>
            <hr />
            <LoadingImage url={props.url} />
        </div>
        
    )
}

export default PokemonCard;