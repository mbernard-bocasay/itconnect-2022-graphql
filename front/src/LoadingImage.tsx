import React, { useEffect, useState } from 'react';
import LoadingGIF from './assets/loading.gif';

export interface LoadingImageInterface {
    url?:string
}

const LoadingImage:React.FunctionComponent<LoadingImageInterface> = (props) => {

    const [loaded,isLoaded] = useState(false);

    useEffect(() => {
        if(props.url){
            isLoaded(false);
            var downloadingImage = new Image();
            downloadingImage.onload = function(){
               isLoaded(true);
            };
            downloadingImage.src = props.url;
        }
        return ()=>{downloadingImage.onload= function(){}}
    },[props.url])
    return (
        <img src={loaded?props.url:LoadingGIF}/>
    )
}

export default LoadingImage;