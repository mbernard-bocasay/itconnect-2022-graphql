export default interface IPokemonCard{
    number: number,
    name:string,
    image:string
}