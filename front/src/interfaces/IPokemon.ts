import IPokemonTypeGraphQL from "./IPokemonType";

export interface IPokemonREST{
    name:string;  
    number:number;  
    image?:string;  
    height?:string;    
    weight?:string;  
    previous_evolutions?: string[];
    next_evolutions?: string[];
    types?: string[];
    weaknessTypes?: string[];
}
  
export default interface IPokemonGraphQL{
    name:string;  
    number:number;  
    image?:string;  
    height?:string;    
    weight?:string;    
    previous_evolutions?: IPokemonGraphQL[];
    next_evolutions?: IPokemonGraphQL[];
    pokemon_types?: IPokemonTypeGraphQL[];
    pokemon_weakness_types?: IPokemonTypeGraphQL[];
}