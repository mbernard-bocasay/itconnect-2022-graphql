import IPokemonGraphQL from "./IPokemon";

export interface IPokemonTypeREST{
    id:number;
    name:string;
    type_pokemons?: string[];
    weak_pokemons?: string[];
    uri?:string;
  }
  
export default interface IPokemonTypeGraphQL{
    id:number;
    name:string;
    type_pokemons?: IPokemonGraphQL[];
    weak_pokemons?: IPokemonGraphQL[];
}