import axios from "axios";
import { IGraphQLResponse } from "../interfaces/IGraphQLResponse";
import IPokemonGraphQL from "../interfaces/IPokemon";
import IPokemonCard from "../interfaces/IPokemonCard";

const BASE_URL = 'http://localhost:3010/graphql';

class GraphQLService {
    
    async getPokemons():Promise<IPokemonGraphQL[]>{
      
      const response = await axios.post<IGraphQLResponse>(BASE_URL, {
        query: `
        query getPokemons {
          pokemons {
            number,
            name,
            image
          }
        }
        `
      });        
        return response.data.data.pokemons;
    }

    async getPokemon(id:number):Promise<IPokemonGraphQL>{
      
      const response = await axios.post<IGraphQLResponse>(BASE_URL, {
        query: `
        query getPokemon {
          pokemon(id:${id}) {
            number,
            name,
            image,
            weight,
            height,
            pokemon_types{
              name
            },
            pokemon_weakness_types{
              name
            }

          }
        }
        `
      });        
        return response.data.data.pokemon;
    }

    async getWeakPokemons(id:number):Promise<IPokemonCard[]>{
      
      const response = await axios.post<IGraphQLResponse>(BASE_URL, {
        query: `
        query getPokemon {
          pokemon(id:${id}) {
            pokemon_types{
              weak_pokemons{
                number,
                name,
                image
              }
            },
          }
        }
        `
      });        

      let reponse = response.data.data.pokemon;
      let pokemons:IPokemonCard[] = [];
      reponse.pokemon_types.forEach((type:any) => {
        type.weak_pokemons.forEach((pokemon:any) => {
          pokemons.push({name:pokemon.name, image:pokemon.image, number:pokemon.number})
        });
      });
      
      pokemons = pokemons.filter((item,
        index) => pokemons.indexOf(item) === index);
      return pokemons;
      
    }

}

const graphqlService = new GraphQLService();

export default graphqlService;