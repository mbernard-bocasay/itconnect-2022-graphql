import axios from "axios";
import { IGraphQLResponse } from "../interfaces/IGraphQLResponse";
import IPokemonGraphQL, { IPokemonREST } from "../interfaces/IPokemon";
import IPokemonCard from "../interfaces/IPokemonCard";
import { IPokemonTypeREST } from "../interfaces/IPokemonType";
import { IRESTResponse } from "../interfaces/IRESTResponse";

const BASE_URL = 'http://localhost:3010';

class RESTService {
    
    async getPokemons():Promise<IPokemonREST[]>{
      
      const response = await axios.get<IRESTResponse>(BASE_URL+"/api/pokemon");        
        return response.data.data;
    }

    async getPokemon(id:number):Promise<IPokemonREST>{
      
      const response = await axios.get<IRESTResponse>(BASE_URL+"/api/pokemon/"+id);        
        return response.data.data;
    }

    
    async getTypes():Promise<IPokemonTypeREST[]>{
      
      const response = await axios.get<IRESTResponse>(BASE_URL+"/api/type");        
        return response.data.data;
    }

    async getType(id:number):Promise<IPokemonTypeREST>{
      
      const response = await axios.get<IRESTResponse>(BASE_URL+"/api/type/"+id);        
        return response.data.data;
    }

    async getWeakPokemons(id:number):Promise<IPokemonCard[]>{

        let weaks:IPokemonREST[] = [];
       // get le pokemon
       let current = await restService.getPokemon(id);
       // get tout les types 
       let types = current.types;
       if(types){
         for await (const type of types) {
            const response = await axios.get<IRESTResponse>(BASE_URL+type);
            let currenttype:IPokemonTypeREST = response.data.data;
            if(currenttype.weak_pokemons){
              for await (const weak of currenttype.weak_pokemons) {
                const response = await axios.get<IRESTResponse>(BASE_URL+weak);
                let currentpokemon:IPokemonREST = response.data.data;
                weaks.push(currentpokemon);
              }
            }
         }
       }

      let pokemons:IPokemonCard[] = [];
      weaks.forEach((pokemon:any) => {
        pokemons.push({name:pokemon.name, image:pokemon.image, number:pokemon.number})
      });

      pokemons = pokemons.filter((item,
        index) => pokemons.indexOf(item) === index);
      return pokemons;
    }

      
 
}

const restService = new RESTService();

export default restService;