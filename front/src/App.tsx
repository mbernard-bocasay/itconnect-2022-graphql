import axios from 'axios';
import { useEffect, useRef, useState } from 'react'
import './App.css'
import IPokemonGraphQL from './interfaces/IPokemon';
import IPokemonCard from './interfaces/IPokemonCard';
import { IRESTResponse } from './interfaces/IRESTResponse';
import LoadingImage from './LoadingImage';
import PokemonCard from './PokemonCard';
import graphqlService from './services/GraphQL.service';
import restService from './services/REST.service';

function App() {

  const [graphtimer, setGraphtimer] = useState<number>(0);
  const [resttimer, setResttimer] = useState<number>(0);
  

  const [pokemons, setPokemons] = useState<IPokemonGraphQL[]>();
  const [pokemon, setPokemon] = useState<IPokemonGraphQL>();
  const [weakPokemons, setWeakPokemons] = useState<IPokemonCard[]>();
  const listRef = useRef<HTMLSelectElement>(null);

  useEffect(() => { 
    graphqlService.getPokemons().then( (datas) => setPokemons(datas) );
    
   }, []);

  const selectPokemon = (e:React.ChangeEvent<HTMLSelectElement>) => {
    graphqlService.getPokemon(parseInt(e.target.value)).then( (data) => setPokemon(data) );
    setWeakPokemons([]);
    setGraphtimer(0);
    setResttimer(0);
  }

  const getStrongREST = async () => {
    if(pokemon){
      setGraphtimer(0);
      setResttimer(0);
      let start = new Date().getTime();
      setWeakPokemons([]);
      let data = await restService.getWeakPokemons(pokemon?.number);
      setWeakPokemons(data)
      let end = new Date().getTime();
      console.log(end-start);
      setResttimer(end-start);
    }
  }

  const getStrongGRAPHQL = async () => {
    if(pokemon){
      setGraphtimer(0);
      setResttimer(0);
      let start = new Date().getTime();
      setWeakPokemons([]);
      let data = await graphqlService.getWeakPokemons(pokemon?.number);
      setWeakPokemons(data) 
      let end = new Date().getTime();
      console.log(end,start);

      setGraphtimer(end-start);
    }
  }

  const onClickCard = (id:number)=> {
    graphqlService.getPokemon(id).then( (data) => {
        
    setGraphtimer(0);
    setResttimer(0);
        setWeakPokemons([]);
        if(listRef.current)
          listRef.current.value = id.toString();
        setPokemon(data)}
       );
  }

  return (
    <>
      { pokemons?.length == 0 ? <div>loading ...</div> :
      <div className="App">
        <a href="http://localhost:3010/api/pokemon" target={'_blank'}>ouvrir API REST</a><br />
        <a href="http://localhost:3010/graphiql" target={'_blank'}>ouvrir API GRAPHQL</a><br />
        <select ref={listRef} onChange={selectPokemon} defaultValue={pokemon?.number}>
          
         <option value="" disabled selected>Choix du pokemon</option>
          {pokemons?.map((pokemon:IPokemonGraphQL) => (
            <option key={pokemon.number} value={pokemon.number}>{pokemon.name}</option>
          ))}
        </select><br />
        {pokemon ? 
        <div>
          <br />
          <div style={{display:'flex', justifyContent:'center'}}>
          <PokemonCard name={pokemon.name} url={pokemon.image}></PokemonCard>
          </div>    
          <p>Types : {pokemon.pokemon_types?.map( (type,index, types) => {
            
            return index === types.length -1 ? type.name : type.name + ", ";
          })}</p>  
           <p>Faible contre les types : {pokemon.pokemon_weakness_types?.map( (type,index, types) => {
            
            return index === types.length -1 ? type.name : type.name + ", ";
          })}</p>  
          <div>
            <h3>Fort contre {weakPokemons && weakPokemons?.length > 0 ? (weakPokemons.length + " pokemons") :' '}</h3>
            <button onClick={getStrongGRAPHQL}>Avec GraphQL { graphtimer && graphtimer > 0 ? graphtimer + " ms" : ''}</button>  &nbsp;&nbsp;&nbsp;&nbsp;
            <button onClick={getStrongREST}>Avec REST { resttimer && resttimer > 0 ? resttimer + " ms" : ''}</button>
            {weakPokemons && weakPokemons?.length > 0 ? 
            <div style={{display:'flex', flexWrap:'wrap', justifyContent:'space-evenly'}}>
              {weakPokemons.map(pokemon => {
                return  <PokemonCard key={pokemon.number} name={pokemon.name} url={pokemon.image} onClick={() => {
                  onClickCard(pokemon.number)
                }}></PokemonCard>
              })}
              
            </div> : 
            <></>
            }
          </div>
        </div>
        : <></>
        }
      </div>
      }
    </>
  )
}

export default App
